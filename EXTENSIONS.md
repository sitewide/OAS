# OpenAPI Extensions #

This document describes the [OpenAPI extensions](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.3.md#specification-extensions) used for Sitewide's [internal OpenAPI specifications](https://gitlab.com/sitewide/OAS).
