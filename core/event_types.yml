######################################################################################
#
# OpenAPI v3.0 fragment specification
#
# @see https://swagger.io/docs/specification/about/
# @see https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.3.md
#
######################################################################################

#
# Required meta-data to ensure fragments are fully OASv3.0 compliant.
#
# @note Empty 'paths' object signals OAS file is a fragment and not an API spec.
#
openapi: 3.0.3
info:
  title: Envelope schemas for events
  #
  # Version identifier for the specification detailed in this OpenAPI fragment.
  #
  version: "2023.12"
paths: {}
#
# Reusable objects for different aspects of the OpenAPI specification document. All
# objects defined within the components object will have no effect on the API unless
# they are explicitly referenced from properties outside the components object.
#
# @see versions/3.0.3.md#componentsObject
#
components:
  schemas:
    #
    # Top level event envelope schema based on the CloudEvents specification.
    #
    # @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md
    #
    CloudEventsCore:
      properties:
        specversion:
          type: string
          description: |
            The version of the CloudEvents specification which the event uses. Compliant event
            producers MUST use a value of '1.0' when referring to this version of the specification.
            Currently, this attribute will only have the 'major' and 'minor' version numbers included
            in it. This allows for 'patch' changes to the specification to be made without changing
            this property's value in the serialization. Note: for 'release candidate' releases a
            suffix might be used for testing purposes.
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md#specversion
          nullable: false
          default: "1.0"

        type:
          type: string
          description: |
            This attribute contains a value describing the type of event related to the originating
            occurrence. Often this attribute is used for routing, observability, policy enforcement,
            etc. The format of this is PRODUCER DEFINED and might include information such as the
            version of the type - see Versioning of CloudEvents in the Primer for more information.
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md#type
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/primer.md#versioning-of-cloudevents
          nullable: false

        source:
          type: string
          description: |
            Identifies the context in which an event happened. Often this will include information
            such as the type of the event source, the organization publishing the event or the
            process that produced the event. The exact syntax and semantics behind the data encoded
            in the URI is defined by the event producer.
            Producers MUST ensure that source + id is unique for each distinct event.
            An application MAY assign a unique source to each distinct producer, which makes it easy
            to produce unique IDs since no other producer will have the same source. The application
            MAY use UUIDs, URNs, DNS authorities or an application-specific scheme to create unique
            source identifiers.
            A source MAY include more than one producer. In that case the producers MUST collaborate
            to ensure that source + id is unique for each distinct event.
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md#source-1
          nullable: false

        id:
          type: string
          description: |
            Identifies the event. Producers MUST ensure that source + id is unique for each distinct
            event. If a duplicate event is re-sent (e.g. due to a network error) it MAY have the same
            id. Consumers MAY assume that Events with identical source and id are duplicates.
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md#id
          nullable: false

        subject:
          type: string
          description: |
            OPTIONAL. This describes the subject of the event in the context of the event producer
            (identified by source). In publish-subscribe scenarios, a subscriber will typically subscribe
            to events emitted by a source, but the source identifier alone might not be sufficient as a
            qualifier for any specific event if the source context has internal sub-structure.
            Identifying the subject of the event in context metadata (opposed to only in the data
            payload) is particularly helpful in generic subscription filtering scenarios where
            middleware is unable to interpret the data content.
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md#subject
          nullable: false

        time:
          type: string
          description: |
            OPTIONAL. Timestamp of when the occurrence happened. If the time of the occurrence cannot
            be determined then this attribute MAY be set to some other time (such as the current time)
            by the CloudEvents producer, however all producers for the same source MUST be consistent
            in this respect. In other words, either they all use the actual time of the occurrence or
            they all use the same algorithm to determine the value used.
            If present, MUST adhere to the format specified in RFC 3339 (ISO-8601).
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md#time
            @see https://datatracker.ietf.org/doc/html/rfc3339
          nullable: false

        datacontenttype:
          type: string
          description: |
            OPTIONAL. Content type of data value. This attribute enables data to carry any type of content,
            whereby format and encoding might differ from that of the chosen event format. For example, an
            event rendered using the JSON envelope format might carry an XML payload in data, and the consumer
            is informed by this attribute being set to 'application/xml'. The rules for how data content is
            rendered for different datacontenttype values are defined in the event format specifications;
            for example, the JSON event format defines the relationship in section 3.1. of the JSON Event
            Format for CloudEvents specification.
            In some event formats the datacontenttype attribute MAY be omitted. For example, if a JSON format
            event has no datacontenttype attribute, then it is implied that the data is a JSON value conforming
            to the 'application/json' media type. In other words: a JSON-format event with no datacontenttype
            is exactly equivalent to one with datacontenttype='application/json'.
            If present, MUST adhere to the format specified in RFC 2046.
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md#datacontenttype
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/formats/json-format.md
            @see https://tools.ietf.org/html/rfc2046
          nullable: false
          #
          # Default to 'text/plain' encoding that allows for text data and base64 encoded binaries.
          #
          default: text/plain
          example:
            - "application/json"
            - "application/xml"
            - "application/avro"
            - "application/vnd.sitewide.api+json;version=2023.12"

        dataschema:
          type: string
          description: |
            OPTIONAL. Identifies the schema that data adheres to. Incompatible changes to the schema
            SHOULD be reflected by a different URI. See Versioning of CloudEvents in the Primer for more
            information.
            If present, MUST be a non-empty URI.
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md#dataschema
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/primer.md#versioning-of-cloudevents
          nullable: false

        data:
          #
          # Default to type 'string' to match the default 'datacontenttype' of 'text/plain'. This
          # property SHOULD be overridden by derived schemas to reflect the 'object' structure of
          # the derived type.
          #
          type: string
          description: |
            OPTIONAL. The event payload. This specification does not place any restriction on the type of
            this information. It is encoded into a media format which is specified by the datacontenttype
            attribute (e.g. application/json), and adheres to the dataschema format when those respective
            attributes are present.
            Event data is domain-specific information about the occurrence (i.e. the payload). This might
            include information about the occurrence, details about the data that was changed, or more.
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md#event-data
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/extensions/dataref.md#dataref
          nullable: false

      required:
        - specversion
        - type
        - source
        - id

    #
    # CloudEvents 1.0 DataRef ('Claim Check Pattern') Extension
    #
    # @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/extensions/dataref.md#dataref
    #
    CloudEventsDataRefExtension:
      properties:
        dataref:
          type: string
          description: |
            OPTIONAL. The dataref attribute MAY be used to reference another location where this information
            is stored. The information, whether accessed via data or dataref MUST be identical.
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/extensions/dataref.md#dataref
          nullable: false

    #
    # CloudEvents 1.0 Partitioning Extension
    #
    # @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/extensions/partitioning.md
    #
    CloudEventsPartitionKeyExtension:
      properties:
        partitionkey:
          type: string
          description: |
            OPTIONAL. A partition key for the event, typically for the purposes of defining a causal
            relationship/grouping between multiple events. In cases where the CloudEvent is delivered
            to an event consumer via multiple hops, it is possible that the value of this attribute
            might change, or even be removed, due to protocol semantics or business processing logic
            within each hop.
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/extensions/partitioning.md#partitionkey
          nullable: false

    #
    # CloudEvents 1.0 Distributed Tracing Extension
    #
    # @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/extensions/distributed-tracing.md
    #
    CloudEventsDistributedTracingExtension:
      properties:
        traceparent:
          type: string
          description: |
            OPTIONAL. Embeds context from Distributed Tracing so that distributed systems can include
            traces that span an event-driven system. This extension is meant to contain historical data
            of the parent trace, in order to diagnose eventual failures of the system through tracing
            platforms like Jaeger, Zipkin, etc. The traceparent property contains a version, trace ID,
            span ID, and trace options as defined in section 3.2 of the W3C Trace Context for HTTP
            specfication.
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/extensions/distributed-tracing.md#tracestate
            @see https://w3c.github.io/trace-context/#traceparent-header
            @see https://w3c.github.io/trace-context/
          nullable: false

        tracestate:
          type: string
          description: |
            OPTIONAL. Embeds context from Distributed Tracing so that distributed systems can include
            traces that span an event-driven system. This extension is meant to contain historical data
            of the parent trace, in order to diagnose eventual failures of the system through tracing
            platforms like Jaeger, Zipkin, etc. The tracestate property contains a comma-delimited list
            of key-value pairs, defined by section 3.3 of the W3C Trace Context for HTTP specfication.
            @see https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/extensions/distributed-tracing.md#tracestate
            @see https://w3c.github.io/trace-context/#tracestate-header
            @see https://w3c.github.io/trace-context/
          nullable: false

    #
    # Sitewide CloudEvents extension for affinity based filtering.
    #
    SitewideProcessingLabelExtension:
      properties:
        affinity:
          type: string
          description: |
            OPTIONAL. Descriptor for use with affinity selection and processing of the occurrence.
            The affinity property contains a comma delimited list of PRODUCER defined strings that
            identify where this event occurrence SHOULD be processed or forwarded to. The 'where' of
            the event processing MAY describe a geographic datacenter label or a customer-specific
            identifier that determines which customer managed system to redirect the event occurrence.
            The affinity property MAY also reflect any legal or privacy constraints on the geographic
            storage or processing of the event data.
          nullable: false

        environment:
          type: string
          description: |
            OPTIONAL. Descriptor for use with cluster environment selection and processing of the
            occurrence. The environment property contains a comma delimited list of PRODUCER defined
            strings that identify whether the cluster environment consuming this event occurrence
            SHOULD handle the data.
            The environment property MAY also reflect any legal or privacy constraints on the storage
            or processing enivornment of the event data.
          nullable: false

    #
    # Sitewide CloudEvents extension for capturing provenance metadata.
    #
    SitewideDataProvenanceExtension:
      properties:
        sourceversion:
          type: string
          description: |
            OPTIONAL. Embeds context information regarding the software or system platform that
            produced the event occurrence. The application MAY use semver or git hash strings as
            identifiers, or an application-specific schema to uniquely describe version information.
          nullable: false

        sourceinstance:
          type: string
          description: |
            OPTIONAL. Embeds context information regarding the particular instance of the software
            system, or device that produced the event occurrence. The application MAY use UUIDs,
            URNs, DNS authorities or an application-specific scheme to create unique instance
            identifiers.
          nullable: false

        sourceserver:
          type: string
          description: |
            OPTIONAL. Embeds context information regarding the node or system running the PRODUCER
            software as identified by the 'sourceversion' and 'sourceinstance' properties. The
            application SHOULD use URNs or DNS authorities to identify such systems.
          nullable: false

        sourceclient:
          type: string
          description: |
            OPTIONAL. Embeds context information regarding any agents or clients of the PRODUCER
            that MAY have precipitated the generation of the event occurrence. The application MAY
            use UUIDs, URNs, DNS authorities, IP addresses, or an application-specific scheme to
            identify such clients.
          nullable: false

    #
    # CloudEvents 1.0 combined schema with all extensions.
    #
    CloudEvent:
      allOf:
        #
        # CloudEvents core and official extensions.
        #
        - $ref: "#/components/schemas/CloudEventsCore"
        - $ref: "#/components/schemas/CloudEventsDataRefExtension"
        - $ref: "#/components/schemas/CloudEventsPartitionKeyExtension"
        - $ref: "#/components/schemas/CloudEventsDistributedTracingExtension"
        #
        # Vendor specific extensions.
        #
        - $ref: "#/components/schemas/SitewideProcessingLabelExtension"
        - $ref: "#/components/schemas/SitewideDataProvenanceExtension"
