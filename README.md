# OpenAPI Specifications #

This repository contains [OpenAPI](https://www.openapis.org/) descriptions for [Sitewide's REST API](https://sitewide.dev).

## What is OpenAPI? ##

From the [OpenAPI Specification](https://github.com/OAI/OpenAPI-Specification):

> The OpenAPI Specification (OAS) defines a standard, programming language-agnostic interface description for HTTP APIs, which allows both humans and computers to discover and understand the capabilities of a service without requiring access to source code, additional documentation, or inspection of network traffic. When properly defined via OpenAPI, a consumer can understand and interact with the remote service with a minimal amount of implementation logic. Similar to what interface descriptions have done for lower-level programming, the OpenAPI Specification removes guesswork in calling a service.

## Description Formats ##

Each OpenAPI document SHOULD be available in a number formats: **bundled** and **fully referenced**.

  - The bundled descriptions are single file artifacts that make usages of OpenAPI **components** for reuse and portability. This SHOULD be the preferred way for 3rd-party developers to interact with Sitewide's OpenAPI description.
  - Our internal build tooling expects fully **referenced** directory structures for easier browsing and development. It MAY be shared here to aid with our own internal development.

## Vendor Extensions ##

Vendor extensions are used for concepts that are harder to express with OpenAPI components and/or are specific to [Sitewide](https://sitewide.dev). For more information on the extensions used in these OpenAPI descriptions, please refer to [EXTENSIONS.md](EXTENSIONS.md)

## Limitations ##

  - Not all (optional) HTTP headers are described in the OpenAPI documents, expect those to be added over time.
  - A lot of operations described in these documents MAY be accessible through multiple paths. At a minimum, the most common path SHOULD be provided to access these operations, but work is underway to describe all possible paths.
  - Relative path [$refs](https://swagger.io/docs/specification/using-ref/) used with fully referenced specifications are constructed in a way to workaround [bugs](https://github.com/OpenAPITools/openapi-generator/issues/6320) and [limitations](https://github.com/OpenAPITools/openapi-generator/issues/10320) with [OpenAPI generator](https://github.com/OpenAPITools/openapi-generator). Relative path references might not be portable across other OpenAPI tooling.

## License ##

[sitewide/OAS](https://gitlab.com/sitewide/OAS) is licensed under the [Apache License 2.0](LICENSE)
